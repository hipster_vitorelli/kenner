import mongoose from 'mongoose';

class Database {
  constructor() {
    this.init();
  }

  init() {
    try {
      this.mongoConnection = mongoose.connect(
        'mongodb://rodrigovitorelli:Rodrigo13@mongo_kennerteste:27017/kennermongo',
        {
          useNewUrlParser: true,
          useFindAndModify: true,
          useUnifiedTopology: true,
        }
      );
    } catch (error) {
      console.log(error);
    }
  }
}

export default new Database();
