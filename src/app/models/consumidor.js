import mongoose from 'mongoose';

const ConsumidoresSchema = new mongoose.Schema(
  {
    cliente: {
      type: Object,
      required: true,
      nome: {
        type: String,
        required: true,
      },
      data_nascimento: {
        type: Date,
        required: true,
      },
      cpf: {
        type: String,
        required: true,
      },
      genero: {
        type: String,
      },
      email: {
        type: String,
      },
      contato: {
        type: Object,
        telefone_fixo: {
          type: Object,
          ddd: {
            type: Number,
          },
          numero: {
            type: String,
          },
        },
        telefone_celular: {
          type: Object,
          ddd: {
            type: Number,
          },
          numero: {
            type: String,
          },
        },
      },
      endereco: {
        type: Object,
        cep: { type: String },
        logradouro: { type: String },
        n_residencia: { type: String },
        complemento: { type: String },
        bairro: { type: String },
        cidade: { type: String },
        estado: { type: String },
      },
    },
    aceita_termos: {
      type: Boolean,
      required: true,
      default: true,
    },
    outras_informacoes: {
      type: Array,
    },
  },
  {
    timestamps: true,
  }
);

export default mongoose.model('Consumidor', ConsumidoresSchema);
