import axios from 'axios';

const api = axios.create({
  baseURL: `http://hmg.s2holding.com.br/selohmg`,
  headers: {
    Authorization: `Bearer 123456789`,
  },
});

export default api;
