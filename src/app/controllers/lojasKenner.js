import api from '../middleware/api';

class Loja {
  async consultarLoja(req, res) {
    const { cnpj } = req.query;
    try {
      const consulta = await api.get(
        `/api/loja/BuscarCNPJ?CNPJLoja=${cnpj}&token=${api.defaults.headers.Authorization}`
      );
      return res.status(200).json(consulta.data);
    } catch (error) {
      return res.status(400).json({ error: 'Error' });
    }
  }
}

export default new Loja();
