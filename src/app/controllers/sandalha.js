import api from '../middleware/api';

class Produto {
  async consultarProduto(req, res) {
    const { selo } = req.query;
    try {
      const consulta = await api.get(
        `/api/SandaliaLinha?codigo=${selo}&token=${api.defaults.headers.Authorization}`
      );
      return res.status(200).json(consulta.data);
    } catch (error) {
      return res.status(400).json({ error: 'Error' });
    }
  }

  async consultarTamanho(req, res) {
    const { id } = req.query;
    try {
      const selo = await api.post(
        `/api/SandaliaLinha/${id}?token=${api.defaults.headers.Authorization}`
      );
      return res.status(200).json(selo.data);
    } catch (error) {
      return res.status(400).json({ error: 'Error' });
    }
  }
}

export default new Produto();
