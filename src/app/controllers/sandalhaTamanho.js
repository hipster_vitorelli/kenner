import api from '../middleware/api';

class Tamanho {
  async consultarTamanho(req, res) {
    const { id } = req.query;
    try {
      const consulta = await api.get(
        `api/SandaliaTamanho/${id}?token=${api.defaults.headers.Authorization}`
      );
      return res.status(200).json(consulta.data);
    } catch (error) {
      return res.status(400).json({ error: 'Error' });
    }
  }
}

export default new Tamanho();
