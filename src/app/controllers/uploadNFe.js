import Consumidor from '../models/consumidor';

class UploadNFeController {
  async Update(req, res) {
    const { id } = req.query;
    const { originalname: nome_original, filename: novo_nome } = req.file;
    const consumidor = await Consumidor.findById(id);

    const {
      IdTipoConsumidor,
      AtributoKenner,
      IdCanalComunicao,
      GrauContribuicaoSelo,
      data_compra,
      cnpj,
      linha_sandalha,
    } = req.body;

    if (consumidor) {
      const { _id } = consumidor;
      try {
        const consumidor = await Consumidor.updateMany(
          { _id },
          {
            $push: {
              outras_informacoes: {
                nota_fiscal: {
                  nome_original,
                  novo_nome,
                  url: `http://localhost:3000/files/${novo_nome}`,
                },
                loja: {
                  cnpj,
                },
                pedido: {
                  IdTipoConsumidor,
                  AtributoKenner,
                  IdCanalComunicao,
                  GrauContribuicaoSelo,
                  data_compra,
                  linha_sandalha,
                  status_selo: 'pendente',
                },
              },
            },
          },
          { new: true }
        );
        res.status(200).json({ message: 'Arquivo inserido com sucesso' });
      } catch (error) {
        res.status(400).json(error);
      }
    } else {
      res
        .status(400)
        .json({ error: 'Não encontramos nenhum usuário com o ID enviado' });
    }
  }
}

export default new UploadNFeController();
