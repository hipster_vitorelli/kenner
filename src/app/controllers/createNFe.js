import Consumidor from '../models/consumidor';

class ConsumidorController {
  async Store(req, res) {
    const { cliente, outras_informacoes, aceita_termos } = req.body;
    const valida = cliente.cpf;

    const clienteExistente = await Consumidor.find({
      'cliente.cpf': valida,
    });

    // CRIA UM CLIENTE QUANDO NÃO HÁ REGISTROS NO BANCO PELO CPF
    if (clienteExistente.length === 0) {
      try {
        const consumidor = await Consumidor.create({
          cliente,
          outras_informacoes,
          aceita_termos,
        });
        res.status(200).json(consumidor);
      } catch (error) {
        res.status(400).json(error);
      }
      // ATUALIZA UM CLIENTE QUANDO HÁ REGISTROS NO BANCO PELO CPF
    } else {
      const id = clienteExistente[0]._id;
      try {
        const consumidor = await Consumidor.findByIdAndUpdate(
          id,
          {
            cliente,
            aceita_termos,
          },
          { new: true }
        );
        res.status(200).json(consumidor);
      } catch (error) {
        res.status(400).json(error);
      }
    }
  }
}

export default new ConsumidorController();
