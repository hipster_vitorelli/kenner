import api from '../middleware/api';

class Selo {
  async ConsultaSelo(req, res) {
    const { selo } = req.query;
    try {
      const consulta = await api.get(
        `/api/Selo?codigo=${selo}&token=${api.defaults.headers.Authorization}`
      );
      return res.status(200).json(consulta.data);
    } catch (error) {
      return res.status(400).json({ error: 'Error' });
    }
  }

  async CreateSelo(req, res) {
    try {
      const selo = await api.post(
        `/api/selo?token=${api.defaults.headers.Authorization}`
      );
      return res.status(200).json(selo.data);
    } catch (error) {
      return res.status(400).json({ error: 'Error' });
    }
  }
}

export default new Selo();
