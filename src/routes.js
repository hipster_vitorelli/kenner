import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';
import createNFe from './app/controllers/createNFe';
import uploadNFe from './app/controllers/uploadNFe';
import Selo from './app/controllers/getSelo';
import Produto from './app/controllers/sandalha';
import Tamanhos from './app/controllers/sandalhaTamanho';
import Loja from './app/controllers/lojasKenner';

const routes = new Router();
const upload = multer(multerConfig);

// routes.use(authMiddleware);
routes.post('/consumidor', createNFe.Store);
routes.post('/files', upload.single('fileUpload'), uploadNFe.Update);
routes.post('/selo', Selo.CreateSelo);
routes.get('/selo', Selo.ConsultaSelo);
routes.get('/produto/tipo', Produto.consultarProduto);
routes.get('/produto/tipo/tamanho', Produto.consultarTamanho);
routes.get('/produto/tipo/tamanho/lista', Tamanhos.consultarTamanho);
routes.get('/loja', Loja.consultarLoja);
module.exports = routes;
