import express from 'express';
import cors from 'cors';
import path from 'path';
import bodyParser from 'body-parser';
import routes from './routes';
import './database';

class App {
  constructor() {
    this.server = express();
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(cors());
    this.server.use(express.json());
    this.server.use(bodyParser.json({ type: 'application/*+json' }));
    this.server.use(bodyParser.json({ limit: '50mb' }));
    this.server.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp', 'uploads'))
    );
  }

  routes() {
    this.server.use(routes);
  }
}

module.exports = new App().server;
